# Constant Config
Build, freeze, and use configs

## Installation

```bash
$ npm install @parthar/const-config --save
```

## Usage

```js
// create
var ConstConfig = require("@parthar/const-config");
var config = new ConstConfig();

// methods
var cfg;
var cfg1 = {a:1, b:2, nested:{n:"i am nested"}};
var cfg2 = {b:22, c:3, arr:[0, 1, {k:"object inside array"}]};
var cfg3 = {z:26};
/* 
    method: assign
    clone & copy own enumerable properties from left to right
    empty object is used for uncloneable values such as error objects, functions, DOM nodes, and WeakMaps
*/
config.assign(cfg1, cfg2, ...); // think Object.assign with target as config object
config.isFrozen(); // boolean: false
config.get('a'); // 1; first get freezes object
config.isFrozen(); // boolean: true
config.get('b'); // 22
config.get('nested.n'); // "i am nested"
config.get('arr[2].k'); // "object inside array"
config.get('non-existent-key'); // throws ReferenceError
config.assign(cfg3); // assign after freeze throws a TypeError
cfg = config.get('arr[2]'); // get JSON ref
cfg.k = "try to modify"; // modify via ref after freeze throws a TypeError
```
