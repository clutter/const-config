"use strict";

const loClone = require("lodash.clonedeep");
const loGet = require("lodash.get");

function deepFreeze(obj) {
    Object.freeze(obj);
    if (obj instanceof Array || obj instanceof Object) {
        Object.getOwnPropertyNames(obj).forEach(function each(key) {
            let val = obj[key];

            deepFreeze(val);
        });
    }
}

class ConstConfig {
    constructor() {
        this._isFrozen = false;
        this._config = {};
    }

    assign() {
        let args = [];

        Array.from(arguments).forEach(function eachArg(a) {
            args.push(loClone(a));
        });
        args.unshift(this._config);
        Object.assign.apply(null, args);
    }

    get(key) {
        let val;

        if (!this._isFrozen) {
            deepFreeze(this._config);
            this._isFrozen = true;
        }

        val = loGet(this._config, key);
        if (!val) {
            throw new ReferenceError("invalid get to: " + key);
        }

        return val;
    }

    isFrozen() {
        return this._isFrozen;
    }
}

module.exports = ConstConfig;
