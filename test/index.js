/* globals before, after, describe, it */
/*eslint func-names: ["error", "never"]*/
/*eslint no-magic-numbers: "off"*/

"use strict";

const assert = require("assert");
const ConstConfig = require("../");

var config;
var cfg1 = {
    "a": 1,
    "b": 2,
    "nested": {
        "n": "i am nested"
    }
};
var cfg2 = {
    "b": 22,
    "c": 3,
    "arr": [0, 1, {
        "k": "object inside array"
    }]
};
var cfg3 = {
    "z": 26
};

before(function (done) {
    config = new ConstConfig();
    done();
});

describe("ConstConfig", function () {
    it("should be a class", function (done) {
        assert.strictEqual(typeof ConstConfig, "function");
        done();
    });
    it("should not be frozen", function (done) {
        assert.strictEqual(config.isFrozen(), false);
        done();
    });
    it("should assign values", function (done) {
        assert.doesNotThrow(function () {
            config.assign(cfg1, cfg2, cfg3);
        });
        done();
    });
    it("should get values", function (done) {
        assert.strictEqual(config.get("a"), 1);
        assert.strictEqual(config.get("b"), 22);
        assert.equal(config.get("nested.n"), "i am nested");
        assert.equal(config.get("arr[2].k"), "object inside array");
        done();
    });
    it("should be frozen after get", function (done) {
        assert.strictEqual(config.isFrozen(), true);
        done();
    });
    it("should throw ReferenceError on get for non-existent key", function (done) {
        assert.throws(function () {
            config.get("non-existent-key");
        }, ReferenceError);
        done();
    });
    it("should throw TypeError on assign after frozen", function (done) {
        let cfg;

        assert.throws(function () {
            config.assign(cfg1);
        }, TypeError);
        assert.throws(function () {
            cfg = config.get("arr[2]");
            cfg.k = "try to modify Object via ref";
        }, TypeError);
        assert.throws(function () {
            cfg = config.get("arr[2]");
            cfg.new = "try to add to Object via ref";
        }, TypeError);
        assert.throws(function () {
            cfg = config.get("arr");
            cfg[0] = "try to modify Array via ref";
        }, TypeError);
        assert.throws(function () {
            cfg = config.get("arr");
            cfg.push = "try to add to Array via ref";
        }, TypeError);
        done();
    });
});

after(function (done) {
    config = null;
    done();
});
